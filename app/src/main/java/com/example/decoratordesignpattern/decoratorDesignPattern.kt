package com.example.decoratordesignpattern

//class `decoratorDesignPattern` {

//}

interface MilkShake {
    fun getTaste()
}

class ConcreteMilkShake : MilkShake {
    override fun getTaste() {
        println("Es leche !")
    }
}
open class MilkShakeDecorator(protected var milkShake: MilkShake) : MilkShake {
    override fun getTaste() {
        this.milkShake.getTaste()
    }
}

class BananaMilkShake(m:MilkShake) : MilkShakeDecorator(m){

    override public fun getTaste(){
        super.getTaste ();
        this.addTaste();
        println(" ¡Es un batido de leche de plátano!");
    }
    public fun addTaste(){
        println(" ¡Añadiendo sabor a plátano al batido de leche!");
    }
}

public class PeanutButterMilkShake(m:MilkShake) : MilkShakeDecorator(m){

    override public fun getTaste(){
        super.getTaste ();
        this.addTaste();
        println(" Es un batido de mantequilla de maní!");
    }
    public fun addTaste(){
        println("¡Agregando sabor a mantequilla de maní al batido de leche!");
    }
}

fun main(args: Array<String>) {
    val peanutMilkShake = PeanutButterMilkShake(ConcreteMilkShake())
    peanutMilkShake.getTaste()
    val bananaMilkShake = BananaMilkShake(ConcreteMilkShake())
    bananaMilkShake.getTaste()
}